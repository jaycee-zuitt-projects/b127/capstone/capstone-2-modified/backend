const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController')
const auth = require('../auth');

router.post('/addToCart', auth.verify, (req,res) => {
	const data = {
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}

	if(!data.isAdmin){
		cartController.createCart(data).then(result => res.send(result))
	}else {
		res.send(false)
	}

})



router.get('/userCart', (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	if (!data.isAdmin) {
		cartController.myCart(data).then(result => res.send(result))
	} else {
		res.send(false);
	}
})



router.put('/removeProduct', (req,res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	} 
	if (!data.isAdmin) {
		cartController.removeProduct(data).then(result => res.send(result))
	} else {
		res.send(false)
	}
})


module.exports = router;
