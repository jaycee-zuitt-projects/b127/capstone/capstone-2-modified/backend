const Cart = require('../models/cartModel');
const Order = require('../models/orderModel');


module.exports.createOrderProto = async (data) => {
	
	let isOrderSaved = await Cart.findOne({userId: data.userId}).then(result => {

		if (result !== null) {
			let newOrder = new Order({
				totalPrice: result.totalPrice,
				products: result.products,
				userId: data.userId
			})
	
			return newOrder.save().then((result,isError)=> {
				return(isError)? false: true;
			})
		} else {
			return false;
		}
	})

	let isCartDeleted = await Cart.findOneAndRemove({userId: data.userId}).then((result,isError) => {
		return (result !== null)? true : false;
	})

	if (isOrderSaved && isCartDeleted) {
		return true;
	} else {
		return false;
	}
}

module.exports.getAllOrdersProto = () => {
	return Order.find({}).then(result => {
		return result
	})
}


module.exports.myOrdersProto = (data) => {
	return Order.find({userId: data.userId}).then(result =>{
		let productArr=[];
		result.forEach(item=>{
			productArr.push(item.products)
		})
		return productArr.flat();
	});
}


// const Order = require('../models/orderModel');

// //create
// /*module.exports.order = async (data) => {
// 	const totalamount = await Order.aggregate([
//         { $match: { quantity: "$quantity" }},
//         {
//             $set: {
//                 quantity: { $quantity: "$quantity"},
//                 price: "$price",
//             },
//         },
//         {
//             $group: {
//                 _id: "$orderId",
//                 amount: { $avg: { $multiply: [ "$price", "$stock" ]}}
//             }
//         }
//     ])

//     let newOrder = new Order({
//         userId: data.Order.userId,
//         products: [
//             {
//                 productId: data.Order.products[0].id,
//                 quantity: data.Order.products[0].quantity,
//                 price: data.Order.products[0].price
//             }
//         ],
//         amount: totalamount,
//         address: data.Order.address,
//     })
//     return newOrder.save().then((Order, error) => {
//         if(error){
//             console.log(error);
//             return false;
//         }else{
//             return Order;
//         }
//     })
// }*/
// module.exports.order = async (data) => {
// 	let newOrder = new Order({
//         userId: data.Order.userId,
//         products: [
//             {
//                 productId: data.Order.products[0].id,
//                 quantity: data.Order.products[0].quantity
//             }
//         ],
//         amount: data.Order.amount,
//         address: data.Order.address,
//     })
//     return newOrder.save().then((Order, error) => {
//         if(error){
//             console.log(error);
//             return false;
//         }else{
//             return Order;
//         }
//     })
// }

// //Authenticated User retrieval orders
// module.exports.myOrders = (data) => {
// 	return Order.find({userId: data.userId}).then(result =>{
// 		return result;
// 	})
// }

// //get all orders admin only
// module.exports.allOrders = (data) => {
//     return Order.find({}).then(result => {
//         return result;
//     })
// }